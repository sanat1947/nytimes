
package com.nytimes.model;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class Headline {

    @Nullable
    @SerializedName("main")
    @ColumnInfo(name = "main")
    private String main;

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

}
