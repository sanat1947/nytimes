
package com.nytimes.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.nytimes.datalayer.source.local.DataConverter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(tableName = "articles", indices = {@Index(value = {"article_id"}, unique = true)})
public class Article {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "db_id")
    private int dbId;

    @Nullable
    @SerializedName("web_url")
    @ColumnInfo(name = "web_url")
    private String webUrl;

    @Nullable
    @SerializedName("snippet")
    @ColumnInfo(name = "snippet")
    private String snippet;

    @Nullable
    @SerializedName("leadParagraph")
    @ColumnInfo(name = "leadParagraph")
    private String leadParagraph;

    @Nullable
    @SerializedName("_abstract")
    @ColumnInfo(name = "_abstract")
    @Ignore
    private String _abstract;

    private String source;
    @TypeConverters(DataConverter.class)
    private List<Multimedium> multimedia = null;
    @Embedded
    private Headline headline;
    private String pubDate;
    private String documentType;
    private String newsDesk;
    private String sectionName;
    private String subsectionName;

    // The byline object does sometimes not conform to the specification: it is sometimes an
    // empty array [] instead of an object {}. If this is the case, the Retrofit call fails
    // (calls onFailure rather than onResponse). By removing the byline field from the model,
    // this problem can be bypassed.
    //private Byline byline;
    private String typeOfMaterial;
    @SerializedName("id")
    @ColumnInfo(name = "article_id")
    private String id;

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getLeadParagraph() {
        return leadParagraph;
    }

    public void setLeadParagraph(String leadParagraph) {
        this.leadParagraph = leadParagraph;
    }

    public String getAbstract() {
        return _abstract;
    }

    public void setAbstract(String _abstract) {
        this._abstract = _abstract;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<Multimedium> getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(List<Multimedium> multimedia) {
        this.multimedia = multimedia;
    }

    public Headline getHeadline() {
        return headline;
    }

    public void setHeadline(Headline headline) {
        this.headline = headline;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getNewsDesk() {
        return newsDesk;
    }

    public void setNewsDesk(String newsDesk) {
        this.newsDesk = newsDesk;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSubsectionName() {
        return subsectionName;
    }

    public void setSubsectionName(String subsectionName) {
        this.subsectionName = subsectionName;
    }

//    public Byline getByline() {
//        return byline;
//    }
//
//    public void setByline(Byline byline) {
//        this.byline = byline;
//    }

    public String getTypeOfMaterial() {
        return typeOfMaterial;
    }

    public void setTypeOfMaterial(String typeOfMaterial) {
        this.typeOfMaterial = typeOfMaterial;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    @Nullable
    public String get_abstract() {
        return _abstract;
    }

    public void set_abstract(@Nullable String _abstract) {
        this._abstract = _abstract;
    }
}
