
package com.nytimes.model;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class Multimedium {

    @Nullable
    @SerializedName("url")
    @ColumnInfo(name = "url")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
