package com.nytimes.presentationLayer.articlesList;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nytimes.R;
import com.nytimes.listener.EndlessRecyclerViewScrollListener;
import com.nytimes.model.Article;
import com.nytimes.util.MessageHandler;
import com.nytimes.util.PicassoHandler;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;

public class ArticlesListFragment extends DaggerFragment implements ArticlesContract.View {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout swipeToRefresh;
    @BindView(R.id.message_container)
    RelativeLayout messageContainer;
    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.layout_container)
    BlurView blurView;
    @Inject
    ArticlesContract.Presenter mPresenter;
    @Inject
    PicassoHandler picassoHandler;
    private ArticlesListAdapter adapter;
    private boolean savedState = false;
    private EndlessRecyclerViewScrollListener mScrollListener;


    @Inject
    public ArticlesListFragment() {
        // Requires empty public constructor
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.articles_list, container, false);
        ButterKnife.bind(this, mainView);
        mPresenter.setActivity(getActivity());
        adapter = new ArticlesListAdapter(new ArrayList<>(0), mPresenter, picassoHandler);
        initViews();
        GridLayoutManager gridLayoutManager;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        else
            gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        recyclerView.setLayoutManager(gridLayoutManager);

        mScrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                ArticlesFilter filter = getType();
                filter.savePage(page);

                mPresenter.setBasicInitFromOtherPlace(filter, true);
                mPresenter.loadArticles();
            }
        };

        recyclerView.addOnScrollListener(mScrollListener);

        //noinspection ConstantConditions
        swipeToRefresh.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark),
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent)
        );
        swipeToRefresh.setOnRefreshListener(() -> {
            getType().savePage(0);
            swipeToRefresh.setRefreshing(true);
            mPresenter.setBasicInit(true, true);
            mPresenter.loadArticles();
        });

        return mainView;
    }

    private void initViews() {
        float radius = 10;
        //noinspection ConstantConditions
        View decorView = getActivity().getWindow().getDecorView();
        //Activity's root View. Can also be root View of your layout (preferably)
        ViewGroup rootView = decorView.findViewById(android.R.id.content);
        //set background, if your root layout doesn't have one
        Drawable windowBackground = decorView.getBackground();
        blurView.setVisibility(View.GONE);
        blurView.setupWith(rootView)
                .windowBackground(windowBackground)
                .blurAlgorithm(new RenderScriptBlur(getActivity()))
                .blurRadius(radius);
    }

    private void showMessageError() {
        messageContainer.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showArticles(ArrayList<Article> articles, boolean setAdapter) {
        if (swipeToRefresh != null)
            swipeToRefresh.setRefreshing(false);
        recyclerView.setVisibility(View.VISIBLE);
        messageContainer.setVisibility(View.GONE);
        adapter.replaceData(articles);
        if (setAdapter)
            recyclerView.setAdapter(adapter);
    }

    @Override
    public void showNoArticles() {
        if (swipeToRefresh != null)
            swipeToRefresh.setRefreshing(false);
        errorMessage.setText("No Articles found");
        showMessageError();
    }

    @Override
    public void showServerError(String error) {
        if (swipeToRefresh != null)
            swipeToRefresh.setRefreshing(false);
        MessageHandler.alertDialog(getActivity(), error, null);
    }

    @Override
    public void showNoInternetConnection() {
        if (swipeToRefresh != null)
            swipeToRefresh.setRefreshing(false);
        Toast.makeText(getActivity(), R.string.no_internet_error_message, Toast.LENGTH_LONG).show();
    }

    public void setType(ArticlesFilter articlesSortType, boolean setAdapter, boolean reload) {
        mPresenter.setAdvancedInit(articlesSortType, setAdapter, false);
        if (reload)
            mPresenter.loadArticles();
    }

    public ArticlesFilter getType() {
        return new ArticlesFilter(getActivity());
    }

    @Override
    public void makeBackgroundBlur() {
        //noinspection ConstantConditions
        Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if (vibrator != null)
            vibrator.vibrate(50);
        blurView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStop() {
        super.onStop();
        savedState = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
        if (blurView.getVisibility() == View.VISIBLE || savedState) {
            savedState = false;
            blurView.setVisibility(View.GONE);
            mPresenter.setBasicInit(false, false);
        }
        mPresenter.loadArticles();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.dropView();  //prevent leaking activity in
        // case presenter is orchestrating a long running task
    }
}

