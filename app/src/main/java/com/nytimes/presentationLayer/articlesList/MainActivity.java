package com.nytimes.presentationLayer.articlesList;

import android.app.DialogFragment;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;

import com.nytimes.R;
import com.nytimes.dialog.FilterDialogFragment;
import com.nytimes.util.ActivityUtils;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity {
    public static final String TAG_FILTER_DIALOG = "filter";
    public static final String EXTRA_ARTICLE_URL = "url";

    private SearchView searchView;
    private MenuItem menuItem;

    ArticlesListFragment articlesFragment;

    @Inject
    Lazy<ArticlesListFragment> articlesFragmentProvider;

    @Inject
    ArticlesFilter articlesFilter;

    @Inject
    ArticlesContract.Presenter mPresenter;

    MenuItem mFilterMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create the article Fragment
        articlesFragment =
                (ArticlesListFragment) getSupportFragmentManager().findFragmentById(R.id.list_container);
        if (articlesFragment == null) {
            articlesFragment = articlesFragmentProvider.get();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), articlesFragment, R.id.list_container);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        setupSearchView(menu);
        // Save reference to the "Filter" menu item and tint icon if any filters are set
        mFilterMenuItem = menu.findItem(R.id.action_filter);
        if (isAnyFilterSet()) tintFilterIcon(true);
        return super.onCreateOptionsMenu(menu);
    }

    public void tintFilterIcon(boolean addTint) {
        Drawable drawable = mFilterMenuItem.getIcon();
        if (drawable != null) {
            drawable.mutate();
            if (addTint)
                drawable.setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            else
                drawable.setColorFilter(null);
        }
    }

    public boolean isAnyFilterSet() {
        return !articlesFilter.getBeginDate().isEmpty()
                || !articlesFilter.getEndDate().isEmpty()
                || !articlesFilter.getNewsType().isEmpty();
    }

    private void setupSearchView(Menu menu) {
        menuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            // Called when query is submitted (by pressing "Search" button on keyboard)
            // Note: empty search queries detected by the SearchView itself and ignored
            @Override
            public boolean onQueryTextSubmit(String query) {
                articlesFilter.saveQuery(query);
                articlesFilter.savePage(0);
                searchView.clearFocus();
                mPresenter.setBasicInitFromOtherPlace(articlesFilter, true);
                mPresenter.loadArticles();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                DialogFragment newFragment = new FilterDialogFragment();
                newFragment.show(getFragmentManager(), TAG_FILTER_DIALOG);
                return true;
            default:
                return false;
        }
    }

    public void applyFilter(){
        searchView.requestFocus();
        menuItem.expandActionView();
    }

}

