package com.nytimes.presentationLayer.articlesList;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nytimes.R;
import com.nytimes.model.Article;
import com.nytimes.model.Multimedium;
import com.nytimes.util.Constants;
import com.nytimes.util.PicassoHandler;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class ArticlesListAdapter extends RecyclerView.Adapter<ArticlesListAdapter.MyViewHolder>{
    private ArrayList<Article> articles;
    private ArticlesContract.Presenter presenter;
    private PicassoHandler picassoHandler;

    ArticlesListAdapter(ArrayList<Article> articles, ArticlesContract.Presenter presenter, PicassoHandler picassoHandler) {
        this.articles = articles;
        this.presenter = presenter;
        this.picassoHandler = picassoHandler;
    }

    void replaceData(ArrayList<Article> articles) {
        setList(articles);
        notifyDataSetChanged();
    }

    private void setList(ArrayList<Article> articles) {
        if (articles!=null)
            this.articles = articles;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.article_poster)
        ImageView articlePoster;
        @BindView(R.id.article_title)
        TextView articleTitle;
        @BindView(R.id.item_container)
        CardView cardView;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_container)
        void onItemClicked() {
            presenter.goToDetailsActivity(articles.get(getAdapterPosition()));
        }

        @OnLongClick(R.id.item_container)
        boolean onItemLongClick(){
            return true;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_article, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Article article = articles.get(position);

        ArrayList<Multimedium> multimedia = (ArrayList<Multimedium>) article.getMultimedia();
        String thumbUrl = "";
        for (Multimedium m : multimedia) {
           // if (m.getType().equals("image") && m.getSubtype().equals("thumbnail")) {
                thumbUrl = Constants.API_IMAGE_BASE_URL + m.getUrl();
                break;
            //}
        }
        if (!thumbUrl.isEmpty())
            // TODO: Glide seems to not cache most of these images but load them from the URL each time
            picassoHandler.getPicasso()
                    .load(Constants.API_IMAGE_BASE_URL + article.getMultimedia().get(0).getUrl())
                    .placeholder(R.drawable.placeholder_thumb)
                    .error(R.drawable.error_thumb)
                    .into(holder.articlePoster);

        holder.articleTitle.setText(article.getHeadline().getMain());
    }

    @Override
    public int getItemCount() {
        return articles!=null?articles.size():0;
    }
}
