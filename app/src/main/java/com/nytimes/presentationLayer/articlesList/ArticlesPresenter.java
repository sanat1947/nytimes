package com.nytimes.presentationLayer.articlesList;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;

import com.nytimes.datalayer.ArticlesDataSource;
import com.nytimes.datalayer.ArticlesRepository;
import com.nytimes.di.scopes.ActivityScope;
import com.nytimes.model.ApiResponse;
import com.nytimes.model.Article;
import com.nytimes.presentationLayer.DetailActivity;
import com.nytimes.util.ProgressDialog;

import java.util.ArrayList;

import javax.inject.Inject;

@ActivityScope
public class ArticlesPresenter implements ArticlesContract.Presenter {
    @Nullable
    private ArticlesContract.View views;
    private ArticlesFilter filter;
    private ArticlesRepository articlesRepository;
    private boolean setAdapter = true;
    private boolean forceUpdate;
    @Nullable
    private Activity activity;

    @Inject
    ArticlesPresenter(ArticlesRepository articlesRepository) {
        this.articlesRepository = articlesRepository;
    }

    @Override
    public void goToDetailsActivity(Article article) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(MainActivity.EXTRA_ARTICLE_URL, article.getWebUrl());
        activity.startActivity(intent);
    }

    @Override
    public void loadArticles() {
        if(filter == null){
            filter = new ArticlesFilter(activity);
            startLoading(filter, forceUpdate,true);
        }else {
            startLoading(filter, forceUpdate, true);
        }
    }

    private void startLoading(ArticlesFilter articlesType, boolean forceUpdate, boolean initialCall){
        if (forceUpdate)
            articlesRepository.refreshData();

        ProgressDialog.showProgressDialog(activity);

        articlesRepository.getArticles(new ArticlesDataSource.LoadArticlesCallback() {
            @Override
            public void noInternetConnection() {
                ProgressDialog.dismissProgressDialog();
                views.showNoInternetConnection();
                onArticlesNotAvailable();
            }

            @Override
            public void onResponse(String TAG, Object response) {
                articlesRepository.deleteAllArticles();
                articlesRepository.saveArticles(((ApiResponse) response).getResponse().getArticles());
                    startLoading(articlesType, false, false);
            }

            @Override
            public void onErrorResponse(String TAG, ApiResponse response) {
                ProgressDialog.dismissProgressDialog();
                views.showServerError(response.getStatus());
            }

            @Override
            public void onArticlesLoaded(ArrayList<Article> articles) {
                ProgressDialog.dismissProgressDialog();
                if (articles.size() > 0) {
                    views.showArticles(articles, setAdapter);
                } else {
                    views.showNoArticles();
                }
            }

            @Override
            public void onArticlesNotAvailable() {
                ProgressDialog.dismissProgressDialog();
                        views.showNoArticles();
            }
        }, articlesType);

    }

    @Override
    public void setAdvancedInit(ArticlesFilter filter, boolean setAdapter, boolean forceUpdate) {
        this.filter = filter;
        this.setAdapter = setAdapter;
        this.forceUpdate = forceUpdate;
    }

    @Override
    public void setBasicInit(boolean setAdapter, boolean forceUpdate) {
        this.setAdapter = setAdapter;
        this.forceUpdate = forceUpdate;
    }

    @Override
    public void setBasicInitFromOtherPlace(ArticlesFilter filter, boolean forceUpdate) {
        this.filter = filter;
        this.forceUpdate = forceUpdate;
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }


    @Override
    public void takeView(ArticlesContract.View view) {
        this.views = view;
    }

    @Override
    public void dropView() {
        this.views = null;
    }
}