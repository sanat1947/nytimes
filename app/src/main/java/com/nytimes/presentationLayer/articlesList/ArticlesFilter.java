package com.nytimes.presentationLayer.articlesList;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class ArticlesFilter {

    private SharedPreferences preferences;
    private static final String QUERY_KEY = "query";
    private static final String FILTERED_QUERY_KEY = "filtered_query";
    private static final String BEGIN_DATE = "begin_date";
    private static final String END_DATE = "begin_date";
    private static final String SORT_ORDER = "sort";
    private static final String NEWS_TYPE = "type";
    private static final String PAGE = "page";

    @Inject
    public ArticlesFilter(Context context){
        preferences = context.getSharedPreferences("NYTIMES", Context.MODE_PRIVATE);
    }

    public void saveQuery(String query){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(QUERY_KEY, query);
        editor.commit();
    }

    public String getQuery(){
       return preferences.getString(QUERY_KEY, "");
    }

    public void saveBeginDate(String date){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(BEGIN_DATE, date);
        editor.commit();
    }

    public String getBeginDate(){
        return preferences.getString(BEGIN_DATE, "");
    }

    public void saveType(String type){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(NEWS_TYPE, type);
        editor.commit();
    }

    public String getNewsType(){
        return preferences.getString(NEWS_TYPE, "");
    }

    public void saveFilteredQuery(String filteredQuery){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(FILTERED_QUERY_KEY, filteredQuery);
        editor.commit();
    }

    public String getFilteredQuery(){
        return preferences.getString(FILTERED_QUERY_KEY, "");
    }

    public void saveEndDate(String date){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(END_DATE, date);
        editor.commit();
    }

    public String getEndDate(){
        return preferences.getString(END_DATE, "");
    }

    public void saveSortOrder(String sort){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SORT_ORDER, sort);
        editor.commit();
    }

    public String getSortOrder(){
        return preferences.getString(SORT_ORDER, "");
    }

    public void savePage(int page){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PAGE, page);
        editor.commit();
    }

    public int getPage(){
        return preferences.getInt(PAGE, 0);
    }
}
