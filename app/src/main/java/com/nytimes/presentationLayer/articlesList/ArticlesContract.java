package com.nytimes.presentationLayer.articlesList;

import android.app.Activity;

import com.nytimes.BasePresenter;
import com.nytimes.model.Article;

import java.util.ArrayList;

public interface ArticlesContract {
    interface View{
        void showArticles(ArrayList<Article> articles, boolean setAdapter);
        void showNoArticles();
        void showServerError(String error);
        void showNoInternetConnection();
        void makeBackgroundBlur();
    }

    interface Presenter extends BasePresenter<View> {
        void goToDetailsActivity(Article article);
        void loadArticles();
        void setAdvancedInit(ArticlesFilter filter, boolean setAdapter, boolean forceUpdate);
        void setBasicInit(boolean setAdapter, boolean forceUpdate);
        void setBasicInitFromOtherPlace(ArticlesFilter filter, boolean forceUpdate);
        void setActivity(Activity activity);
    }
}
