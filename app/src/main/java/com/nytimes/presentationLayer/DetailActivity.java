package com.nytimes.presentationLayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.nytimes.R;
import com.nytimes.presentationLayer.articlesList.MainActivity;
import com.nytimes.util.ProgressDialog;

public class DetailActivity extends AppCompatActivity {

    String mUrl;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_detail_activity);

        mUrl = getIntent().getStringExtra(MainActivity.EXTRA_ARTICLE_URL);

        setContentView(R.layout.activity_detail);

        webView = (WebView)findViewById(R.id.webView);

        ProgressDialog.showProgressDialog(this);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                ProgressDialog.dismissProgressDialog();
                super.onPageFinished(view, url);
            }
        });
        webView.loadUrl(mUrl);
    }

    // Make navigation button in app bar behave like the device back button, i.e. return to previous
    // activity without calling onCreate, in this way, search results in MainActivity are maintained
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    // Change the device back button to a back button of the embedded web browser
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, mUrl);

        MenuItem item = menu.findItem(R.id.menu_item_share);
        ShareActionProvider p = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        p.setShareIntent(intent);

        return super.onCreateOptionsMenu(menu);
    }
}
