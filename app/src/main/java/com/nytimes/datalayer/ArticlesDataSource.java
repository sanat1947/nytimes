package com.nytimes.datalayer;

import android.support.annotation.NonNull;

import com.nytimes.datalayer.source.remote.listeners.NetworkListener;
import com.nytimes.model.Article;
import com.nytimes.presentationLayer.articlesList.ArticlesFilter;

import java.util.ArrayList;

public interface ArticlesDataSource {

    interface LoadArticlesCallback extends NetworkListener {
        void onArticlesLoaded(ArrayList<Article> articles);
        void onArticlesNotAvailable();
    }

    default void refreshData(){}
    void getArticles(@NonNull LoadArticlesCallback callback, ArticlesFilter filter);

    default void saveArticles(ArrayList<Article> articles){}

    default void updateArticle(Article article){}

    default void deleteAllArticles(){}



}
