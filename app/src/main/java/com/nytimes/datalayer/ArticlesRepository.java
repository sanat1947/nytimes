package com.nytimes.datalayer;

import android.support.annotation.NonNull;

import com.nytimes.di.scopes.Local;
import com.nytimes.di.scopes.Remote;
import com.nytimes.model.ApiResponse;
import com.nytimes.model.Article;
import com.nytimes.presentationLayer.articlesList.ArticlesFilter;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ArticlesRepository implements ArticlesDataSource {

    private final ArticlesDataSource mArticlesRemoteDataSource;

    private final ArticlesDataSource mArticlesLocalDataSource;

    private boolean mCacheIsDirty = false;

    @Inject
    ArticlesRepository(@Remote ArticlesDataSource articlesRemoteDataSource,
                       @Local ArticlesDataSource articlesLocalDataSource) {
        mArticlesRemoteDataSource = articlesRemoteDataSource;
        mArticlesLocalDataSource = articlesLocalDataSource;
    }

    private void getArticlesFromServer(@NonNull LoadArticlesCallback callback, ArticlesFilter filter) {
        mCacheIsDirty = false;
        mArticlesRemoteDataSource.getArticles(callback, filter);
    }

    @Override
    public void getArticles(@NonNull final LoadArticlesCallback callback, final ArticlesFilter filter) {
        if (mCacheIsDirty)
            getArticlesFromServer(callback, filter);
        else
            mArticlesLocalDataSource.getArticles(new LoadArticlesCallback() {
                @Override
                public void onArticlesLoaded(ArrayList<Article> articles) {
                    callback.onArticlesLoaded(articles);
                }

                @Override
                public void onArticlesNotAvailable() {
                    getArticlesFromServer(callback, filter);
                }

                @Override
                public void noInternetConnection() {
                    // Not required for this calling because this calling only call the local DB.
                }

                @Override
                public void onResponse(String TAG, Object response) {
                    // Not required for this calling because this calling only call the local DB.
                }

                @Override
                public void onErrorResponse(String TAG, ApiResponse response) {
                    // Not required for this calling because this calling only call the local DB.
                }
            }, filter);
    }

    @Override
    public void refreshData() {
        mCacheIsDirty = true;
    }


    @Override
    public void saveArticles(ArrayList<Article> articles) {
        mArticlesLocalDataSource.saveArticles(articles);
    }


    @Override
    public void updateArticle(Article article) {
        mArticlesLocalDataSource.updateArticle(article);
    }

    @Override
    public void deleteAllArticles() {
        mArticlesLocalDataSource.deleteAllArticles();
    }
}
