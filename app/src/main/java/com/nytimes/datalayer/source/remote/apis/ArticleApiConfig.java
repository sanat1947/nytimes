package com.nytimes.datalayer.source.remote.apis;

import com.nytimes.model.ApiResponse;
import com.nytimes.util.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ArticleApiConfig {

    @GET(Constants.ARTICLE_SEARCH)
    Call<ApiResponse> executeArticles(@Query("q") String query,
                                      @Query("fq") String filteredQuery,
                                      @Query("begin-date") String beginDate,
                                      @Query("end-date") String endDate,
                                      @Query("page") int page,
            @Query(Constants.PARAM_API_KEY) String apiKey);
}
