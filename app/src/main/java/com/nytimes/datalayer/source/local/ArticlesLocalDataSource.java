package com.nytimes.datalayer.source.local;

import android.support.annotation.NonNull;

import com.nytimes.datalayer.ArticlesDataSource;
import com.nytimes.model.Article;
import com.nytimes.presentationLayer.articlesList.ArticlesFilter;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ArticlesLocalDataSource implements ArticlesDataSource {
    private ArticlesDAO articlesDAO;


    @Inject
    ArticlesLocalDataSource(@NonNull ArticlesDAO articlesDAO) {
        this.articlesDAO = articlesDAO;
    }

    @Override
    public void getArticles(@NonNull LoadArticlesCallback callback, ArticlesFilter filter) {
        ArrayList<Article> articles = new ArrayList<Article>((Collection<? extends Article>) articlesDAO.selectArticles());
        if (articles.size() > 0)
            callback.onArticlesLoaded(articles);
        else
            callback.onArticlesNotAvailable();
    }

    @Override
    public void refreshData() {
    }

    @Override
    public void saveArticles(ArrayList<Article> articles) {
        for (Article article : articles) {
            articlesDAO.insertArticle(article);
        }
    }

    @Override
    public void updateArticle(Article article) {
        articlesDAO.updateArticle(article);
    }

    @Override
    public void deleteAllArticles() {
        articlesDAO.deleteAll();
    }
}
