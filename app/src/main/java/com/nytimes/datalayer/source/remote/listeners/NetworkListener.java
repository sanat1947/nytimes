package com.nytimes.datalayer.source.remote.listeners;

import com.nytimes.model.ApiResponse;

public interface NetworkListener {
    void noInternetConnection();
    void onResponse(String TAG, Object response);
    void onErrorResponse(String TAG, ApiResponse response);
}
