package com.nytimes.datalayer.source.remote.request;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.Log;

import com.nytimes.datalayer.source.remote.listeners.NetworkListener;
import com.nytimes.model.ApiResponse;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestHandler {

    private static final String TAG = "RequestHandler";

    public static <T> void execute(Call<T> call, NetworkListener networkListener, Context context) {
        execute(TAG, call, networkListener, context);
    }

    private static <T> void execute(final String TAG, Call<T> call, final NetworkListener networkListener, Context context) {
        //check if there is internet connection or not.
        if (checkInternetConnection(context)) {

            // if there is internet connection
            Log.d(TAG, call.request().toString());
            // Fire the interface implementation and get the server response
            call.enqueue(new Callback<T>() {
                @Override
                public void onResponse(@NonNull Call<T> call, @NonNull retrofit2.Response<T> response) {
                    //If the request success! return the response body to the Activity.
                    if (response.errorBody() == null && response.body() != null){
                        networkListener.onResponse(TAG, response.body());
                    }else{
                        ApiResponse apiResponse = new ApiResponse();
                        apiResponse.setStatus("401");
                        networkListener.onErrorResponse(TAG, apiResponse);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
                    //If the request Failed! return the error response to the Activity.
                    ApiResponse apiResponse = new ApiResponse();
                    apiResponse.setStatus("404");
                    networkListener.onErrorResponse(TAG, apiResponse);
                }
            });

        } else {
            // if there is no internet connection call noInternetConnection in the Activity to notify the user.
            networkListener.noInternetConnection();
        }
    }

    private static boolean checkInternetConnection(Context context) {
        ConnectivityManager connectivityManager =

                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static Retrofit getClient(String baseUrl) {

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.addInterceptor(loggingInterceptor);

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create()).build();
    }
}
