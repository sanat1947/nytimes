package com.nytimes.datalayer.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.nytimes.model.Article;

import java.util.List;

@Dao
public interface ArticlesDAO {

    @Query("SELECT * FROM articles")
    List<Article> selectArticles();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertArticle(Article article);

    @Update
    int updateArticle(Article artile);

    @Query("DELETE FROM articles")
    int deleteAll();

}
