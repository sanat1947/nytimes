package com.nytimes.datalayer.source.remote;

import android.app.Application;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;

import com.nytimes.datalayer.ArticlesDataSource;
import com.nytimes.datalayer.source.remote.apis.ArticleApiConfig;
import com.nytimes.datalayer.source.remote.request.RequestHandler;
import com.nytimes.model.Article;
import com.nytimes.presentationLayer.articlesList.ArticlesFilter;
import com.nytimes.util.Constants;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ArticlesRemoteDataSource implements ArticlesDataSource {
    private Application application;

    @Inject
    ArticlesRemoteDataSource(Application application) {
        this.application = application;
    }


    @Override
    public void getArticles(@NonNull LoadArticlesCallback callback, ArticlesFilter filter) {
        ArticleApiConfig articleApiConfig = RequestHandler.getClient(Constants.BASE_URL).create(ArticleApiConfig.class);

        RequestHandler.execute(articleApiConfig.executeArticles(filter.getQuery(), filter.getFilteredQuery(), filter.getBeginDate(), filter.getEndDate(), filter.getPage(), Constants.API_KEY),
                callback,
                application.getApplicationContext()
        );
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void saveArticles(ArrayList<Article> articles) {

    }

    @Override
    public void updateArticle(Article article) {

    }

    @Override
    public void deleteAllArticles() {

    }
}
