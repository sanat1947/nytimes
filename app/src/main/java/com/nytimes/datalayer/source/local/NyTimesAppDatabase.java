package com.nytimes.datalayer.source.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.nytimes.model.Article;

@Database(entities = {Article.class}, version = 1, exportSchema = false)
@TypeConverters({DataConverter.class})
public abstract class NyTimesAppDatabase extends RoomDatabase {

    public abstract ArticlesDAO articleDAO();
}
