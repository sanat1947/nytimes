package com.nytimes.di;

import android.app.Application;

import com.nytimes.ApplicationClass;
import com.nytimes.datalayer.ArticlesRepository;
import com.nytimes.di.modules.ActivityBindingModule;
import com.nytimes.di.modules.ArticlesRepositoryModule;
import com.nytimes.di.modules.PicassoModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component(modules = {ArticlesRepositoryModule.class,
        PicassoModule.class,
        ApplicationModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<ApplicationClass> {

    ArticlesRepository getArticlesRepository();

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}
