package com.nytimes.di.modules;

import android.app.Application;

import com.nytimes.util.PicassoHandler;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class PicassoModule {

    @Provides
    static PicassoHandler providePicassoHandler(Application context) {
        return new PicassoHandler(context);
    }
}
