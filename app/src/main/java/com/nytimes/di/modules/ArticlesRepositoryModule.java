/*
 * Copyright (C) 2018 Shehab Salah Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.nytimes.di.modules;

import android.app.Application;
import android.arch.persistence.room.Room;


import com.nytimes.datalayer.ArticlesDataSource;
import com.nytimes.datalayer.source.local.ArticlesDAO;
import com.nytimes.datalayer.source.local.ArticlesLocalDataSource;
import com.nytimes.datalayer.source.local.NyTimesAppDatabase;
import com.nytimes.datalayer.source.remote.ArticlesRemoteDataSource;
import com.nytimes.di.scopes.Local;
import com.nytimes.di.scopes.Remote;
import com.nytimes.util.AppExecutors;
import com.nytimes.util.DiskIOThreadExecutor;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class ArticlesRepositoryModule {
    private static final int THREAD_COUNT = 3;

    @Singleton
    @Binds
    @Local
    abstract ArticlesDataSource provideLocalDataSource(ArticlesLocalDataSource dataSource);

    @Singleton
    @Binds
    @Remote
    abstract ArticlesDataSource provideRemoteDataSource(ArticlesRemoteDataSource dataSource);

    @Singleton
    @Provides
    static NyTimesAppDatabase provideDb(Application context) {
        return Room.databaseBuilder(
                context.getApplicationContext(),
                NyTimesAppDatabase.class, "NyTimesAppMvpClean.db")
                .allowMainThreadQueries()
                .build();
    }

    @Singleton
    @Provides
    static ArticlesDAO provideArticleDao(NyTimesAppDatabase db) {
        return db.articleDAO();
    }

    @Singleton
    @Provides
    static AppExecutors provideAppExecutors() {
        return new AppExecutors(new DiskIOThreadExecutor(),
                Executors.newFixedThreadPool(THREAD_COUNT),
                new AppExecutors.MainThreadExecutor());
    }
}
