package com.nytimes.di.modules;

import com.nytimes.di.scopes.ActivityScope;
import com.nytimes.presentationLayer.articlesList.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {ArticleModule.class})
    abstract MainActivity articlesActivity();

}
