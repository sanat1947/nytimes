package com.nytimes.di.modules;

import com.nytimes.di.scopes.ActivityScope;
import com.nytimes.di.scopes.FragmentScope;
import com.nytimes.presentationLayer.articlesList.ArticlesContract;
import com.nytimes.presentationLayer.articlesList.ArticlesListFragment;
import com.nytimes.presentationLayer.articlesList.ArticlesPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ArticleModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract ArticlesListFragment articlesListFragment();

    @ActivityScope
    @Binds
    abstract ArticlesContract.Presenter articlePresenter(ArticlesPresenter presenter);
}
