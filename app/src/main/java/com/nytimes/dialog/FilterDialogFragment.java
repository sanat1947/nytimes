package com.nytimes.dialog;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nytimes.R;
import com.nytimes.presentationLayer.articlesList.ArticlesFilter;
import com.nytimes.presentationLayer.articlesList.MainActivity;
import com.nytimes.util.MyDate;
import com.nytimes.util.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import javax.inject.Inject;

public class FilterDialogFragment extends DialogFragment {

    public static final String TAG_BEGIN_DATE = "DatePickerBeginDate";
    public static final String TAG_END_DATE = "DatePickerEndDate";

    public EditText etBeginDate, etEndDate;
    SharedPreferences mPref;
    ArrayList<CheckBox> mNewsTypeCheckboxes;
    private LinearLayout llNewsType;
    private CheckBox cbNewsDesk;
    private TextView tvNewsType, tvClearFilters;

    private int year, month, day;

    @Inject
    public ArticlesFilter filter;

    private static final SimpleDateFormat mOutFormat1 = new SimpleDateFormat("d MMM. yyyy", Locale.US);
    private static final SimpleDateFormat mOutFormat3 = new SimpleDateFormat("EEE. d MMM. yyyy", Locale.US);
    private static final SimpleDateFormat mOutFormat2 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private static final SimpleDateFormat mOutFormat4 = new SimpleDateFormat("yyyyMMdd", Locale.US);

    @Inject
    public FilterDialogFragment(){

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.dialog_filter, null, false);

        etBeginDate = (EditText)view.findViewById(R.id.etBeginDate);
        etEndDate = (EditText)view.findViewById(R.id.etEndDate);

        tvClearFilters = (TextView)view.findViewById(R.id.tvClearFilters);

        etBeginDate.setFocusable(false);
        etEndDate.setFocusable(false);

        if(filter == null){
            filter = new ArticlesFilter(getActivity());
        }

        if(!filter.getBeginDate().equalsIgnoreCase("")){
            Date date = null;
            try {
                date = mOutFormat4.parse(filter.getBeginDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(date == null) {
                etBeginDate.setText("");
            }else {
                String newDate = mOutFormat2.format(date);
                etBeginDate.setText(newDate);
            }
        }

        if(!filter.getEndDate().equalsIgnoreCase("")){
            Date date = null;
            try {
                date = mOutFormat4.parse(filter.getEndDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(date == null) {
                etEndDate.setText("");
            }else {
                String newDate = mOutFormat2.format(date);
                etEndDate.setText(newDate);
            }
        }

        setupNewsType(view);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton("save", null)
                .setNegativeButton("cancel", (dialog1, id) -> {}).create();

        // Override setPositiveButton in order to prevent automatic dismissal of dialog
        dialog.setOnShowListener(d -> {
            Button button = ((AlertDialog) d).getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    writeBeginDateValue();
                    writeEndDateValue();
                    writeNewsDeskValue();
                    //e.apply();

                    // Tint icon of the "Filter" menu item if and only if any filters are set
                    MainActivity a = (MainActivity) getActivity();
                    if (a.isAnyFilterSet()) a.tintFilterIcon(true);
                    else a.tintFilterIcon(false);

                    a.applyFilter();

                    dialog.dismiss();
                }
                
                private void writeBeginDateValue() {
                    Date date = null;
                    try {
                        date = mOutFormat2.parse(etBeginDate.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(date == null) {
                        return;
                    }else {
                        String newDate = mOutFormat4.format(date);
                        filter.saveBeginDate(newDate);
                    }
                }
                private void writeEndDateValue() {
                    Date date = null;
                    try {
                        date = mOutFormat2.parse(etEndDate.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(date == null) {
                        return;
                    }else {
                        String newDate = mOutFormat4.format(date);
                        filter.saveEndDate(newDate);
                    }
                }
                private void writeNewsDeskValue() {
                    String value = "";
                    if (cbNewsDesk.isChecked()) {
                        Iterator itr = mNewsTypeCheckboxes.iterator();
                        while (itr.hasNext()) {
                            CheckBox cb = (CheckBox) itr.next();
                            if (cb.isChecked()) {
                                value += cb.getTag();
                                if (itr.hasNext()) value += ":";
                            }
                        }
                    }
                    filter.saveType(value);
                }
            });
        });

        etBeginDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(getActivity(), pickerListenerBeginDate, year, month,day).show();
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(getActivity(), pickerListenerEndDate, year, month,day).show();
            }
        });

        tvClearFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter.saveBeginDate("");
                filter.saveEndDate("");
                filter.saveType("");

                MainActivity a = (MainActivity) getActivity();
                if (a.isAnyFilterSet()) a.tintFilterIcon(true);
                else a.tintFilterIcon(false);

                a.applyFilter();

                dialog.dismiss();
            }
        });

        // Return the AlertDialog
        return dialog;
    }

    private DatePickerDialog.OnDateSetListener pickerListenerBeginDate = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;

            etBeginDate.setText(new StringBuilder().append(year)
                    .append("-").append(month + 1).append("-").append(day)
                    .append(" "));

        }
    };

    private DatePickerDialog.OnDateSetListener pickerListenerEndDate = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;

            etEndDate.setText(new StringBuilder().append(year)
                    .append("-").append(month + 1).append("-").append(day)
                    .append(" "));

        }
    };

    private void setupNewsType(View view) {
        llNewsType = (LinearLayout)view.findViewById(R.id.newsTypeContainer);
        cbNewsDesk = (CheckBox)view.findViewById(R.id.cbNewsType);
        tvNewsType = (TextView)view.findViewById(R.id.tvNewsType);
        mNewsTypeCheckboxes = new ArrayList<>();
        String[] a = getResources().getStringArray(R.array.news_desk_values);
        for (int i = 0; i < a.length; i++) {
            CheckBox cb = new CheckBox(getActivity());
            cb.setText(a[i]);
            cb.setTag(a[i]);

            if((a[i]+":").equalsIgnoreCase(filter.getNewsType())){
                cb.setChecked(true);
            }else {cb.setChecked(false);}
            llNewsType.addView(cb);
            mNewsTypeCheckboxes.add(cb);
        }

        for (CheckBox cb : mNewsTypeCheckboxes) {
            cb.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked && !cbNewsDesk.isChecked()) cbNewsDesk.setChecked(true);
            });
        }
    }

}
