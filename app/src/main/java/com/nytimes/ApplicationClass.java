package com.nytimes;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

import com.nytimes.datalayer.ArticlesRepository;
import com.nytimes.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class ApplicationClass extends DaggerApplication {
    @Inject
    ArticlesRepository articlesRepository;

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }

    @VisibleForTesting
    public ArticlesRepository getArticlesRepository() {
        return articlesRepository;
    }
}
