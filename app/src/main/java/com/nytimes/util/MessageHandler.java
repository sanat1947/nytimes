package com.nytimes.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class MessageHandler {
    private static AlertDialog.Builder dialogBuilder;
    private static AlertDialog dialog;

    public static void alertDialog(Context mContext, String message, DialogInterface.OnClickListener onClickListener) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setMessage(message);
        if (onClickListener != null) {
            dialogBuilder.setPositiveButton("OK", onClickListener);
        } else {
            dialogBuilder.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                    dialogBuilder = null;
                }
            });
        }

        dialog = dialogBuilder.create();
        if (!((Activity) mContext).isFinishing())
            dialog.show();
    }

}
