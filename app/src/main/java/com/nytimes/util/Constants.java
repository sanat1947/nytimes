package com.nytimes.util;

public class Constants {
    //API KEY
    public static final String API_KEY                  = "uEJImOonG1HwBZdxL76LTag5NcSxBD1n"; // <== TODO! (Add NyTimes API KEY HERE)

    //LINKS
    public static final String BASE_URL                 = "https://api.nytimes.com/svc/search/v2/";
    public static final String ARTICLE_SEARCH           = "articlesearch.json";
    public static final String API_IMAGE_BASE_URL       = "http://www.nytimes.com/";

    //PARAMS
    public static final String PARAM_API_KEY            = "api-key";
}
