package com.nytimes.util;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.nytimes.R;

public final class ProgressDialog {
    private static Dialog progressDialog;

    /**
     * Empty Constructor
     * not called
     */
    private ProgressDialog() {

    }

    /**
     * Is showing boolean.
     *
     * @return the boolean
     */
    public static boolean isShowing() {
        return progressDialog != null && progressDialog.isShowing();
    }

    /**
     * Show progress dialog.
     *
     * @param context the context
     */
    public static void showProgressDialog(final Context context) {
        showProgressDialog(context, null);
    }

    private static void updateProgress(Dialog progressDialog, String message){
        TextView textView = (TextView)progressDialog.findViewById(R.id.title);
        if (message == null) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(message);
        }
    }

    /**
     * Method to show the progress dialog with a message
     *
     * @param context the context
     * @param message the message
     */
    public static void showProgressDialog(final Context context, final String message) {

        try {
            /* Check if the last instance is alive */
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    updateProgress(progressDialog, message);
                    return;
                }
            }
            /* Ends Here */
            progressDialog = new Dialog(context, R.style.progress_dialog_theme);
            progressDialog.setContentView(R.layout.dialog_progress);
            updateProgress(progressDialog, message);
            /*if (message == null) {
                tvMessage.setVisibility(View.GONE);
            } else {
                tvMessage.setVisibility(View.VISIBLE);
                tvMessage.setText(message);
            }
            Animation rotate = AnimationUtils.loadAnimation(context, R.anim.rotate);
            ivLoading.setAnimation(rotate);*/
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Dismisses the Progress Dialog
     *
     * @return the boolean
     */
    public static boolean dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {

                try {
                    progressDialog.dismiss();
                } catch (final Exception ex) {
                    ex.printStackTrace();
                }
                progressDialog = null;
                return true;
            }
        }
        return false;
    }
}
